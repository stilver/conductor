package com.conductor.game.events;


import java.util.Dictionary;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by an-st on 12.11.2017.
 */

public class UpEvent {

    private Map<GameState,Boolean> gameEvent;

    public UpEvent() {

        this.gameEvent = new HashMap<GameState, Boolean>();
        this.gameEvent.put(GameState.NullEvent,true);

    }

    public boolean tryGameEvent(GameState gameEvent) {

        return this.gameEvent.get(gameEvent) != null;
    }

    public void setGameEvent(GameState gameEvent) {

        this.gameEvent.put(gameEvent,true);
    }

    public void removeEvent(GameState gameEvent) {

        if (this.tryGameEvent(gameEvent)) {
            this.gameEvent.remove(gameEvent);

        }

    }

    public void eventStopBus() {
        this.gameEvent.put(GameState.StopBus,true);
    }

    public void eventStartGame() {
        this.gameEvent.put(GameState.StartGame,true);

    }

}