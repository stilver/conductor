package objects;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.XmlReader;
import com.badlogic.gdx.utils.XmlReader.Element;

import java.io.IOException;


public class Bus extends Entity {

    private Integer passengers;

    private Array<BusStop> route;
    private float driveFromStop;
    private int currentStop;
    private boolean special; //Остановка по нажатию
    private static  float ACCELERATOR = 500;
    private boolean stop = true;
    private final static int PAUSE = 500;
    private float timer;
    private float delta = Gdx.graphics.getDeltaTime();


    public Bus(String nameOfRoute, int width, int height) {
        super(new Vector2(30, 10), width, height);
        this.passengers = 0;
        this.special = false;

        this.route = new Array<BusStop>();

        this.driveFromStop = 0;
        this.currentStop = 0;
    }

    public float getDriveFromStop() {

        if (this.driveFromStop < 0) {
            return 0;
        }
        return this.driveFromStop;
    }


    public Bus(int width, int height) throws IOException {

        this("25", width, height);

    }

    public Vector2 getVelocity() {
        return this.velocity;
    }

    public BusStop getCurrentStop () {

        return this.route.get(this.currentStop);
    }

    public BusStop getNextStop () {

        if ((this.currentStop+1) < this.route.size) {
            return this.route.get(this.currentStop + 1);
        }
        return this.route.get(0);
    }


    public boolean tryBusStop() {

        if (driveFromStop >= route.get(currentStop).length-ACCELERATOR*delta){

            currentStop+=1;
            driveFromStop=-ACCELERATOR*delta; //Из-за торможения

            if (currentStop >= route.size-1){
                currentStop = 0;
            }

            return true;

        }

        return false;

    }

    public boolean tryBusStart() {

        if ((isStop()) && (!special)) {
            timer += pause(5);
            if (timer > PAUSE) {
                timer = 0;
                return true;
            }
        }
        return false;

    }

    //Считаем время остановки в секундах
    private float pause(int seconds) {
        return (PAUSE/seconds)*Gdx.graphics.getDeltaTime();
    }

    public void openDoors() {

    }

    public void closeDoors() {

    }

    public boolean isStop() {
        return this.stop;
    }

    public float run(boolean update) {

        if (!stop && update && (getVelocity().x < maxVelocity-ACCELERATOR*delta)) {
            velocity.x+=ACCELERATOR*Gdx.graphics.getDeltaTime();
        } else if (stop && update && (getVelocity().x) > ACCELERATOR*delta) {
            velocity.x-=ACCELERATOR*Gdx.graphics.getDeltaTime();
        } else if (stop && update && (getVelocity().x) < ACCELERATOR*delta) {
            velocity.x = 0;
        }

        driveFromStop+= getVelocity().x/15 * Gdx.graphics.getDeltaTime();
        return getVelocity().x * Gdx.graphics.getDeltaTime();
    }


    public void start() {

        this.stop = false;
        this.special = false;

    }

    public void stop(boolean special) {

        this.stop = true;
        this.special = special;

    }

    public void stop() {
        this.stop = true;
    }

    public void update(float delta) {
        this.position = this.position.add(this.velocity);
    }
}
