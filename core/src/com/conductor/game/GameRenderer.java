package com.conductor.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import helpers.AssetLoader;
import objects.Bus;
import objects.Conductor;
import objects.Passengers.CommonPassenger;

// Класс для рендера объектов игрового мира
public class GameRenderer {

    private GameWorld world;
    private OrthographicCamera camera;

    private SpriteBatch batcher;

    public GameRenderer(GameWorld world) {
        this.world = world;

        camera = new OrthographicCamera();
        camera.setToOrtho(true, Gdx.graphics.getWidth() / 2, Gdx.graphics.getHeight() / 2);
        batcher = new SpriteBatch();
        batcher.setProjectionMatrix(camera.combined);
    }

    public void render(float runTime) {
        Bus bus = world.getBus();
        Conductor conductor = world.getConductor();
        CommonPassenger passenger = world.getCommonPassenger();

        Gdx.gl.glClearColor(0, 0,0,1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        batcher.begin();

        batcher.draw(AssetLoader.bus, bus.getPosition().x, bus.getPosition().y,
                     bus.getWidth(), bus.getHeight());
        batcher.draw(AssetLoader.conductorStay, conductor.getPosition().x,
                     conductor.getPosition().y, conductor.getWidth(), conductor.getHeight());
        batcher.draw(AssetLoader.commonPassenger, passenger.getPosition().x,
                     passenger.getPosition().y, passenger.getWidth(),passenger.getHeight());

        batcher.end();
    }
}
