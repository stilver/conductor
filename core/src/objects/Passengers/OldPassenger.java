package objects.Passengers;

import com.badlogic.gdx.math.Vector2;

import objects.Entity;
import objects.Passenger;

public class OldPassenger extends Entity implements Passenger {

    public OldPassenger(Vector2 position, int width, int height) {
        super(position, width, height);
    }

    public void chooseSeat() {

    }

    public void pay() {

    }

    public void update() {
        this.position = this.position.add(this.velocity);
    }

    public void chooseVictim() {

    }
}
