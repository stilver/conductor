package com.conductor.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

import objects.Bus;
import objects.Conductor;
import objects.Passengers.CommonPassenger;

// Класс, предназначенный для обновления игрового мира
public class GameWorld {

    private Bus bus;
    private Conductor conductor;
    private CommonPassenger commonPassenger;

    public GameWorld() {
        int gameWidth = Gdx.graphics.getWidth();
        int gameHeight = Gdx.graphics.getHeight();
        // Делим на два из-за ортографической камеры
        // Оставляем спейс для остановки и кусков дороги
        bus = new Bus("25", gameWidth / 2 - 35, gameHeight / 2 - 20);

        conductor = new Conductor(new Vector2(50, 200), bus.getWidth() / 5, bus.getHeight() / 7);
        commonPassenger = new CommonPassenger(new Vector2(50, 50), bus.getWidth() / 5, bus.getHeight() / 7);
    }
    public void update(float delta) {
        bus.update(delta);
        conductor.update(delta);
        commonPassenger.update(delta);
    }

    public Bus getBus() {
        return bus;
    }

    public Conductor getConductor() {
        return conductor;
    }

    public CommonPassenger getCommonPassenger() {
        return commonPassenger;
    }
}
