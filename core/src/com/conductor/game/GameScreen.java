package com.conductor.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.conductor.game.events.PopUp;
import com.conductor.game.events.GameState;

import java.io.IOException;

import helpers.InputHandler;
import objects.Bus;

public class GameScreen implements Screen {

    private GameWorld world;
    private GameRenderer render;
    private float runTime;

    public GameScreen() {
        float screenWidth = Gdx.graphics.getWidth();
        float screenHeight = Gdx.graphics.getHeight();
        float gameWidth = 200;
        float gameHeight = screenHeight / (screenWidth / gameWidth);

        runTime = 0;

        world = new GameWorld();
        render = new GameRenderer(world);

        Gdx.input.setInputProcessor(new InputHandler(world.getConductor()));
    }

	@Override
	public void render (float delta) {
        runTime += delta;
        world.update(delta);
        render.render(runTime);
	}


    @Override
    public void show() {

    }


    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

	@Override
	public void dispose () {
	}
}
