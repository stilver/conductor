package objects;

/**
 * Created by an-st on 13.11.2017.
 */

public class BusStop {

    public String title;
    public int length;

    BusStop(String title, int length) {

        this.length = length;
        this.title = title;
    }

}
