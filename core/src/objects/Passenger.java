package objects;

public interface Passenger {
    void chooseSeat();
    void pay();
}
