package objects.Passengers;

import com.badlogic.gdx.math.Vector2;

import objects.Entity;
import objects.Passenger;

public class CommonPassenger extends Entity implements Passenger {

    public CommonPassenger(Vector2 position, int width, int height) {
        super(position, width, height);
    }

    public void chooseSeat() {

    }

    public void pay() {

    }

    @Override
    public void update(float delta) {
        velocity.add(acceleration.cpy().scl(delta));

        if (velocity.x > maxVelocity) {
            velocity.x = maxVelocity;
        }
        if (velocity.y > maxVelocity) {
            velocity.y = maxVelocity;
        }

        position.add(velocity.cpy().scl(delta));
    }
}
