package objects;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;

public class Conductor extends Entity {

    private Integer balance;

    public Conductor(Vector2 position, int width, int height) {
        super (position, width, height);
        balance = 30;
    }

    public void onClick() {
        // just for tests
        acceleration.y = -10;
    }

    public void takePayment() {
        this.balance += 30;
    }

    public Integer getBalance() {
        return this.balance;
    }

    public void setBalance(Integer balance) {
        this.balance = balance;
    }

}
