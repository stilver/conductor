package com.conductor.game.events;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;


/**
 * Created by an-st on 12.11.2017.
 */

public class PopUp {

    private Texture texture;
    private Rectangle rectangle;
    public TextButton closeButton;
    public TextButton drugButton;
    private String str;
    private BitmapFont font;
    private Skin skin;
    private int width;
    private int height;

    public PopUp(int x, int y, BitmapFont font) {

        this.str = "Добро пожаловать";
        this.font = font;
        this.width = x;
        this.height = y;
        Pixmap pixmap = new Pixmap( this.width, this.height, Pixmap.Format.RGBA8888);
        pixmap.setColor(0, 0, 0, 0.45f); //a = 1 непрозрачно
        pixmap.fill();
        texture = new Texture(pixmap);
        rectangle = new Rectangle();
        rectangle.x = (Gdx.graphics.getWidth()-this.width)/2;
        rectangle.y = (Gdx.graphics.getHeight()-this.height)/2;

        createBasicSkin("button-close");
        closeButton = new TextButton("",skin);
        closeButton.setWidth(this.width/15);
        closeButton.setHeight(this.height/11);

        createBasicSkin("button-drug");
        drugButton = new TextButton("",skin);
        drugButton.setWidth((float) (this.width/7.5));
        drugButton.setHeight((float)(this.height/15));
        dialogButtonSetPosition();

    }

    public PopUp(int x, int y, BitmapFont font, String str) {

        this(x, y, font);
        this.str = str;

    }

    private void dialogButtonSetPosition() {

        closeButton.setPosition(rectangle.x+this.width-closeButton.getWidth(),
                rectangle.y+this.height-closeButton.getHeight());

        drugButton.setPosition(rectangle.x+this.width/2-this.drugButton.getWidth()/2,
                rectangle.y+this.height-drugButton.getHeight());

    }

    public void moveOn(float x, float y) {

        rectangle.x= x-this.width/2;
        rectangle.y= y-this.height;

        if (rectangle.x < 0) {
            rectangle.x=0;
        } else if (rectangle.x+this.width >Gdx.graphics.getWidth()) {
            rectangle.x=Gdx.graphics.getWidth()-this.width;
        }
        if (rectangle.y < 0) {
            rectangle.y = 0;
        } else if (rectangle.y+this.height > Gdx.graphics.getHeight()) {
            rectangle.y=Gdx.graphics.getHeight()-this.height;
        }

        dialogButtonSetPosition();

    }

    private void createBasicSkin(String str) {

        //Шрифт
        this.skin = new Skin();
        skin.add("default",font);

        //Текстура
        TextureAtlas buttonAtlas = new TextureAtlas(Gdx.files.internal("menu/buttons.pack"));
        skin.addRegions(buttonAtlas);

        //Стиль для кнопки
        TextButton.TextButtonStyle textButtonStyle = new TextButton.TextButtonStyle();
        textButtonStyle.up = skin.getDrawable(str);
        textButtonStyle.font = skin.getFont("default");
        skin.add("default",textButtonStyle);


    }

    public void draw(SpriteBatch batch) {



        batch.draw(texture,rectangle.x,rectangle.y);
        closeButton.draw(batch,1);
        drugButton.draw(batch,1);
        font.draw(batch,str,rectangle.x + 0.1f*width, 0.8f*height + rectangle.y);

    }


}
