package com.conductor.game;
import com.badlogic.gdx.math.Vector2;


/**
 * Created by an-st on 15.11.2017.
 * Класс столкновений, возвращает 1 при возможном столкновении.
 * В конструтор необходимо добавить положение автобуса и его ширину/высоту
 * На данный момент матрица задается статично,но в дальнейшем планирую добавить подгрузку из карты
 */
public class Collision {

private final static int NX = 3;
private final static int NY = 4;

    //Изображение нужно повернуть на 90 по часовой стрелке, чтобы представить
    private boolean[][] matrix = {
            {false/*(0,0)*/,false,false,false},
            {false,false,false,false},
            {true,true,true,true}
    };
    private Vector2 size;
    private Vector2[][] orig;

    //position - позиция автобуса относительно игры
    Collision (Vector2 position, float width, float height) {

        this.size = new Vector2(width/NX,height/NY);
        orig = new Vector2[NX][NY];
        orig[0][0] = new Vector2(position.x, position.y);


        for (int i = 0;i < NX;i++) {
            for (int j = 0; j < NY; j++) {

                if (i > 0) {
                    orig[i][j] = new Vector2(orig[i-1][j].x+this.size.x, orig[i-1][j].y);
                }
                if (j > 0 ) {
                    orig[i][j] = new Vector2( orig[i][j-1].x,orig[i][j-1].y+this.size.y);
                }
            }
        }
    }


    public boolean getCollision(Vector2 oldPosistion, Vector2 newPosition) {

        if (((newPosition.x-oldPosistion.x) != 0) || ((newPosition.y-oldPosistion.y) != 0)) {

            for (int i = 0;i < NX;i++) {
                for (int j = 0; j < NY; j++) {
                    if ((newPosition.x > orig[i][j].x) && (newPosition.x < orig[i][j].x+size.x)
                     && (newPosition.y > orig[i][j].y) && (newPosition.y < orig[i][j].y+size.y)){
                        return matrix[i][j];
                    }
                }
            }

        }

        return false;
    }

    //установка препятствия в ячейку, если параметр collision = true
    //снятие препятствия, если false
    public void setCollision(Vector2 position, boolean collision) {

        for (int i = 0;i < NX;i++) {
            for (int j = 0; j < NY; j++) {
                if ((position.x > orig[i][j].x) && (position.x < orig[i][j].x+size.x)
                        && (position.y > orig[i][j].y) && (position.y < orig[i][j].y+size.y)){
                    matrix[i][j] = collision;
                }
            }
        }

    }




}
