package com.conductor.game;

import com.badlogic.gdx.Game;

import helpers.AssetLoader;


public class ConductorGame extends Game {

    @Override
    public void create() {
        AssetLoader.load();
        setScreen(new GameScreen());
    }

    @Override
    public void dispose() {
        super.dispose();
        AssetLoader.dispose();
    }
}
