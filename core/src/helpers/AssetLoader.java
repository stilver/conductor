package helpers;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.g3d.model.Animation;


// Класс для загрузки текстуры
// Причем используются статичные поля для использования
// одной копии текстуры в игре
// TODO: Ярик должен сделать текстурную карту, чтобы было пиздато
public class AssetLoader {
    public static Texture busTexture;
    public static TextureRegion bus;

    public static Texture conductorTexture;
    //public static Animation conductorAnimation;
    public static TextureRegion conductorStay;
    //public static TextureRegion conductorRight;
    //public static TextureRegion conductorPay;

    public static Texture passengerTexture;
    public static TextureRegion commonPassenger;

    public static void load() {
        busTexture = new Texture("bus.png");
        busTexture.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
        bus = new TextureRegion(busTexture, 0, 0, 508, 1280);
        bus.flip(false, true);

        conductorTexture = new Texture("conductor/conductor_stay.png");
        conductorTexture.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
        conductorStay = new TextureRegion(conductorTexture, 0, 0, 120, 128);
        conductorStay.flip(false, false);

        passengerTexture = new Texture("passengers/male1.png");
        passengerTexture.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
        commonPassenger = new TextureRegion(passengerTexture, 0, 0, 120, 128);
        commonPassenger.flip(false, true);
    }

    public static void dispose() {
        busTexture.dispose();
        conductorTexture.dispose();
        passengerTexture.dispose();
    }
}
